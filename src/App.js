import './App.css';
import Home from './components/Home';
import Layout from './components/Layout';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Products from './components/Products';
import Temp from './components/Temp';
import Test from './components/Test';
import SearchItems from './components/SearchItems';


function App() {

  return (

        <>

          <BrowserRouter>
            <Routes>
              <Route path="/" element={<Layout/>}>
                <Route index element={<Home />} />
                <Route path="product/:id" element={<Products />} />
                <Route path="search/:cat" element={<SearchItems />} />
                <Route path="t" element={<Temp />} />
                <Route path="test" element={<Test />} />
              </Route>
            </Routes>
          </BrowserRouter>
          

        </>
 );
}

export default App;
