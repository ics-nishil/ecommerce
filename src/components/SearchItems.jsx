import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';

const SearchItems = () => {
    
    const category =  useParams().cat
    const [searchCategory,setSearchCategory] = useState(null)
     
    useEffect(()=>{
        fetch(`https://dummyjson.com/products/search?q=${category}`)
        .then(res => res.json())
        .then(data => setSearchCategory(data));
    }, [category])



  return (
    <>
    <div className="grid grid-cols-3 gap-4">
    
        {searchCategory && searchCategory?.products?.map((e) => 
        
            <div id={e.id}>
                <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                    
                    <img className="rounded-t-lg" src={e.thumbnail} alt="" style={{height: '250px'}} />
                    <div className="p-5">
                        <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{e.title}</h5>
                        
                        <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">{e.description}</p>
                        
                        <div>
                            <span className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">Brand </span>
                            <span className='font-bold'>{e.brand}</span>
                        </div> 
                        
                        <div>
                            <span className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">Price </span>
                            <span className='font-bold'>{e.price}</span>
                        </div> 
                        
                        <button className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                            <Link to={`/product/${e.id}` }>
                                <svg className="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                                    <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
                                </svg>
                            </Link>
                        </button>
                        
                        <br/>
                    </div>
                </div>
             
            </div>

    
        )}
        
    </div>
            

    </>
    
  )
}

export default SearchItems
