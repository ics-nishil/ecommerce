import React, { useState } from 'react'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom';

const Products = () => {


    const params = useParams()
    const [item,setItem]=useState(null)
    
    useEffect(()=>{
        fetch(`https://dummyjson.com/products/${params.id}`)
        .then(res => res.json())
        .then(data => setItem(data));
    },[])

  return (
  <>

    {item && 

      <>
        <div className="flex justify-center relative mx-6 mt-4 h-96 overflow-hidden rounded-xl bg-white bg-clip-border text-gray-700">
          <img src={item.thumbnail} className="h-full object-cover" alt='' />
        </div>
      
        <div className="p-6">
            <p className="block font-sans text-base font-medium leading-relaxed text-blue-gray-900 antialiased">
              {item.title}
            </p>
            <p className="block font-sans text-base font-medium leading-relaxed text-blue-gray-900 antialiased">
              Price : {item.price}
            </p>
            <p className="block font-sans text-sm font-normal leading-normal text-gray-700 antialiased opacity-75">
              Brand : {item.brand}
            </p>
            <p className="block font-sans text-sm font-normal leading-normal text-gray-700 antialiased opacity-75">
              Category : {item.category}
            </p>
            <p className="block font-sans text-sm font-normal leading-normal text-gray-700 antialiased opacity-75">
              Description : {item.description}
            </p>
        </div>
      
        <div className="p-6 pt-0">
          <button style={{backgroundColor: 'black', color: 'white'}}
            className="block w-full select-none rounded-lg bg-blue-gray-900/10 py-3 px-2 text-center align-middle font-sans text-xs font-bold uppercase text-blue-gray-900 transition-all hover:scale-105 focus:scale-105 focus:opacity-[0.85] active:scale-100 active:opacity-[0.85] disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none"
            type="button">
            Add to Cart
          </button>
        </div>
      
        <div className="flex justify-center max-w-7xl mx-auto  px-4 sm:px-6 lg:py-24 lg:px-8">
            <div className="grid grid-cols-1 gap-5 sm:grid-cols-4 mt-4">
                <div className="bg-white overflow-hidden shadow sm:rounded-lg">
                    <div className="px-4 py-5 sm:p-6">
                        <dl>
                            <dt className="text-sm leading-5 font-medium text-gray-500 truncate">Rating</dt>
                            <dd className="mt-1 text-3xl leading-9 font-semibold text-indigo-600">{item.rating}</dd>
                        </dl>
                    </div>
                </div>
                <div className="bg-white overflow-hidden shadow sm:rounded-lg">
                    <div className="px-4 py-5 sm:p-6">
                        <dl>
                            <dt className="text-sm leading-5 font-medium text-gray-500 truncate">Stock</dt>
                            <dd className="mt-1 text-3xl leading-9 font-semibold text-indigo-600">{item.stock}</dd>
                        </dl>
                    </div>
                </div>
                <div className="bg-white overflow-hidden shadow sm:rounded-lg">
                    <div className="px-4 py-5 sm:p-6">
                        <dl>
                            <dt className="text-sm leading-5 font-medium text-gray-500 truncate">Discount</dt>
                            <dd className="mt-1 text-3xl leading-9 font-semibold text-indigo-600">{item.discountPercentage} %</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
      
      </>
}
  
</> 

  )
}

export default Products