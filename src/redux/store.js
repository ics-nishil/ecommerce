import { configureStore } from "@reduxjs/toolkit";
import ecommerceReducer from "./ecommerceSlice"


export default configureStore({
    reducer:{
        items: ecommerceReducer
    }
    
})


// import { configureStore } from "@reduxjs/toolkit";
// import { jsonApi } from "./jsonApi";

// export const store =  configureStore({
//     reducer:{
//         [jsonApi.reducerPath]: jsonApi.reducer
//     },

//     middleware: (getDefaultMiddleware)=>{
//         return getDefaultMiddleware().concat(jsonApi.middleware)
//     }
    
// });

// export default store
