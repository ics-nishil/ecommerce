import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

//Action
export const fetchItems = createAsyncThunk('fetchItems', async()=>{
    const response = await fetch('https://dummyjson.com/products')
    return response.json()
})

export const ecommerceSlice = createSlice({

    name: 'items',
    initialState: { 
        isLoading: false,
        data: null,
        isError: false,
        id: 0
     },

     extraReducers: (builder) => {
        builder.addCase(fetchItems.pending, (state, action)=>{
            state.isLoading = true 
        });

        builder.addCase(fetchItems.fulfilled, (state, action)=> {
            state.isLoading = false;
            state.data = action.payload;
        });

        builder.addCase(fetchItems.rejected, (state, action)=> {
            console.log("ERROR: ", action.payload);
            state.isError = true;
        });
    },
});




export default ecommerceSlice.reducer
